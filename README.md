Cloudflare API v4 Dynamic DNS Update in Bash, without unnecessary requests
Now the script also supports v6(AAAA DDNS Recoards)

----

创建 Cloudflare API 令牌，请转到 https://dash.cloudflare.com/profile/api-tokens 并按照以下步骤操作：

1. 单击创建令牌
2. 为令牌提供一个名称，例如，`cloudflare-ddns`
3. 授予令牌以下权限：
    * 区域 - 区域 - 读取
    * 区域 - 区域设置 - 读取
    * 区域 - DNS - 编辑
4. 将区域资源设置为：
    * 包括 - 特定区域 - 选择你想设置的域名

----


```
bash <(curl -Ls https://git.io/cloudflare-ddns) -k cloudflare-api-key \
 -h host.example.com \     # fqdn of the record you want to update
 -z example.com \          # will show you all zones if forgot, but you need this
 -t A|AAAA                 # specify ipv4/ipv6, default: ipv4



bash ./cloudflare-ddns.sh -k cloudflare-api-key \
  -h host.example.com \
  -z example.com \
  -t A|AAAA



```

# 下载脚本
```
wget -N https://gitlab.com/yuban/cloudflare-api-v4-ddns/-/raw/dev/cf-v4-ddns.sh && bash cf-v4-ddns.sh


chmod +x /root/cf-v4-ddns.sh
```

# 修改 DDNS 脚本并补充相关信息
```
vim cf-v4-ddns.sh
```


```
# incorrect api-key results in E_UNAUTH error
# 填写 Global API Key
CFKEY=

# Username, eg: user@example.com
# 填写 CloudFlare 登陆邮箱
CFUSER=

# Zone name, eg: example.com
# 填写需要用来 DDNS 的一级域名
CFZONE_NAME=

# Hostname to update, eg: homeserver.example.com
# 填写 DDNS 的二级域名(只需填写前缀)
CFRECORD_NAME=
```


# 设置定时任务
```
crontab -e

*/2 * * * * /root/cf-v4-ddns.sh >/dev/null 2>&1

* * * * * /root/2.sh >> /root/cron.log 2>&1


python your_script.py "cdecv" "vdrfvfg"


```
# 如果需要日志，替换上一行代码
```
*/2 * * * * /root/cf-v4-ddns.sh >> /var/log/cf-ddns.log 2>&1

```