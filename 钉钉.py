import base64
import hashlib
import hmac
import json
import os
import re
import threading
import time
import urllib.parse
import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import requests
import sys

DD_BOT_TOKEN = "50ea53d62907467335914010f33251df306059c61afbbbb2ad3e5676da0cdc50"
DD_BOT_SECRET = "SEC960dda6c5159958bf07bf18bd351e90530afa91733f84296a6efce0346c38075"


def dingding_bot(title: str, content: str) -> None:
  """
    使用 钉钉机器人 推送消息。
    """
  if not DD_BOT_SECRET or not DD_BOT_TOKEN:
    print("钉钉机器人 服务的 DD_BOT_SECRET 或者 DD_BOT_TOKEN 未设置!!\n取消推送")
    return
  print("钉钉机器人 服务启动")

  timestamp = str(round(time.time() * 1000))
  secret_enc = DD_BOT_SECRET.encode("utf-8")
  string_to_sign = "{}\n{}".format(timestamp, DD_BOT_SECRET)
  string_to_sign_enc = string_to_sign.encode("utf-8")
  hmac_code = hmac.new(secret_enc,
                       string_to_sign_enc,
                       digestmod=hashlib.sha256).digest()
  sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
  url = f'https://oapi.dingtalk.com/robot/send?access_token={DD_BOT_TOKEN}&timestamp={timestamp}&sign={sign}'
  headers = {"Content-Type": "application/json;charset=utf-8"}
  data = {"msgtype": "text", "text": {"content": f"{title}\n\n{content}"}}
  response = requests.post(url=url,
                           data=json.dumps(data),
                           headers=headers,
                           timeout=15).json()

  if not response["errcode"]:
    print("钉钉机器人 推送成功！")
  else:
    print("钉钉机器人 推送失败！")


if __name__ == "__main__":
  if len(sys.argv) >= 3:
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    dingding_bot(arg1, arg2)
  else:
    print("请提供参数！")
